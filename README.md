# Gomoku GUI and engine
WORK IN PROGRESS but functioning:
A Gomoku (5 in a row) GUI and engine. To run, simply run in a python 3 environment (everything has been moved to one file). To play, click a square to place your white piece. Click again on any vacant square and the engine will think then place somewhere on the board.

# Features that have been implemented:
- Evaluation function (works by counting chains and space available for more chains, weighting)
- Engine that plays move that results in highest evaluation value
- (Suppressed) Depth function that searches multiple moves ahead for the engine


# Features TO implement:
- Alpha beta pruning for depth search optimisation
- Neuron network to optimise evaluation function coefficient
- Highlighting previous move for ease of use

